import javafx.scene.image.Image;

public class Disparo {
	private Image disparo;
	private int posX;
	private int posY;
	private int posXmax;
	private int posYmax;
	private int direccion;
	
	
	public Disparo(int movx, int movy, int direccion) {
		
		this.disparo = new Image("disparo.png",50 ,50, true, false);
		this.posX = movx;
		this.posY = movy;
		this.direccion = direccion;
	}
	
	public void mover() {
		if(direccion==0) {
			moverDerecha();
		}else if(direccion==1) {
			moverAbajo();
		}else if(direccion==2) {
			moverIzquierda();
		}else {
			moverArriba();
		}
	}
	
	public void moverAbajo() {
		posY = posY +20;
	}
	
	public void moverArriba() {
		posY = posY-20;
	}
	
	public void moverDerecha() {
		posX = posX+20;
	}
	
	public void moverIzquierda() {
		posX = posX-20;
	}
	
	public Image getDisparo() {
		return disparo;
	}
	public void setDisparo(Image disparo) {
		this.disparo = disparo;
	}
	public int getMovx() {
		return posX;
	}
	public void setMovx(int movx) {
		this.posX = movx;
	}
	public int getMovy() {
		return posY;
	}
	public void setMovy(int movy) {
		this.posY = movy;
	}
	public float getDireccion() {
		return direccion;
	}

	public int getPosXmax() {
		return posX + 80;
	}

	public void setPosXmax(int posXmax) {
		this.posXmax = posXmax;
	}

	public int getPosYmax() {
		return posY + 50;
	}

	public void setPosYmax(int posYmax) {
		this.posYmax = posYmax;
	}
	
	
}
