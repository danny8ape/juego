import javafx.application.Application;
import javafx.scene.Group;
import javafx.stage.Stage;

public class Main extends Application{

	public static void main(String[] args) {
	launch(args);	
	}
	@Override
	public void start(Stage theStage) throws Exception {
		theStage.setTitle("Juego");
		Nivel nivel = new Nivel(new Group(), 1024, 520);
		theStage.setScene(nivel);
		nivel.runLevel();
		theStage.show();
		
	}
}
