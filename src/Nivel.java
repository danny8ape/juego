import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class Nivel extends Scene implements EventHandler<KeyEvent>{

	private ArrayList<Disparo> listaDisparos;
	private ArrayList<Enemigo> listaEnemigos;

	private Protagonista jugador;
	private int puntaje;
	
	private Puntaje texto;

	private Label vida;
	private Label puntosSalida;
	private Canvas level;
	private Image background;
	private GraphicsContext contex;
	private VBox datos;
	private boolean mostrar;
	

	public Nivel(Group root, float width, float heidth) {
		super(root);
		texto = new Puntaje();
		level = new Canvas(width, heidth);
		jugador = new Protagonista();
		puntaje=0;
		mostrar=true;
		vida = new Label(""+jugador.getSalud());
		vida.setStyle("-fx-base: rgb(0,0,0)");
		puntosSalida = new Label(""+puntaje);
		puntosSalida.setStyle("-fx-base: rgb(0,0,0)");
		datos = new VBox();
		datos.getChildren().addAll(vida,puntosSalida);
		listaDisparos = new ArrayList<>();
		listaEnemigos = new ArrayList<>();
		

		background = new Image("fondo.jpg", 1500, 1500, true, false);
		root.getChildren().addAll(level,datos);
		
		contex = level.getGraphicsContext2D();
		this.setOnKeyPressed(this);
		Enemigo temporal = new Enemigo();
		listaEnemigos.add(temporal);
	}

	
	public boolean destruirEnemigos(Enemigo enemigo, Disparo disparo) {
		
		int x = enemigo.getMovx();
		int y = enemigo.getMovy();
		int Mx = enemigo.getPosXmax();
		int My = enemigo.getPosYmax();
		int m =  disparo.getMovx();
		int n =  disparo.getMovy();
		int Mm = disparo.getPosXmax();
		int Mn = disparo.getPosYmax();
		 		
	 	if((x<=m&&m<=Mx&&y<=n&&n<=My)) {
	 		return true;
		}
		if((x<=m&&m<=Mx&&y<=Mn&&Mn<=My)) {
			return true;
		}
		if((x<=Mm&&Mm<=Mx&&y<=Mn&&Mn<=My)) {
			return true;
		}
		if((x<=Mm&&Mm<=Mx&&y<=n&&n<=My)) {
			return true;
		}

	return false;
	}

	public boolean finJuego(Enemigo enemigo, Protagonista jugador) {
		
		int x = enemigo.getMovx();
		int y = enemigo.getMovy();
		int Mx = enemigo.getPosXmax();
		int My = enemigo.getPosYmax();
		int m =  jugador.getMovx();
		int n =  jugador.getMovy();
		int Mm = jugador.getPosXmax();
		int Mn = jugador.getPosYmax();
		 		
	 	if((x<=m&&m<=Mx&&y<=n&&n<=My)) {
	 		return true;
		}
		if((x<=m&&m<=Mx&&y<=Mn&&Mn<=My)) {
			return true;
		}
		if((x<=Mm&&Mm<=Mx&&y<=Mn&&Mn<=My)) {
			return true;
		}
		if((x<=Mm&&Mm<=Mx&&y<=n&&n<=My)) {
			return true;
		}

	return false;
	}


	public void runLevel() {
		new AnimationTimer() {

			@Override
			public void handle(long arg01) {

				actualizar();
				contex.drawImage(background, 0, 0);
				
				if(mostrar) {
					
					if(listaEnemigos.size()==0) {
						Enemigo temporal1 = new Enemigo((int)(Math.random()*1000),(int)(Math.random()*400));
						listaEnemigos.add(temporal1);
					}
					
					if (jugador.getMovx()>= 1001) {
						jugador.setMovx(0);
					}

					if (jugador.getMovy()>=451) {
						jugador.setMovy(0);
					}

					if (jugador.getMovx()<=-1) {
						jugador.setMovx(1000);
					}

					if (jugador.getMovy()<=-1) {
						jugador.setMovy(450);
					}
					


					for (int i = 0; i < listaEnemigos.size(); i++) {
						listaEnemigos.get(i).movimientoEnemigo(jugador.getMovx(), jugador.getMovy());
						contex.drawImage(listaEnemigos.get(i).getSkin(), listaEnemigos.get(i).getMovx(), listaEnemigos.get(i).getMovy());
					}


					for (int i = 0; i < listaDisparos.size();i++) {
						
						
						if(listaDisparos.size()>0) {
							listaDisparos.get(i).mover();
							contex.drawImage(listaDisparos.get(i).getDisparo(), listaDisparos.get(i).getMovx(), listaDisparos.get(i).getMovy());
						
						}
						if(listaDisparos.get(i).getMovx()>1001||listaDisparos.get(i).getMovx()<-1) {
							System.out.println("f");
							listaDisparos.remove(i);
							break;
						}
						
						if(listaDisparos.get(i).getMovy()>1001||listaDisparos.get(i).getMovy()<-1) {
							listaDisparos.remove(i);
							break;
						}
						
					}
					contex.drawImage(jugador.getSkin(), jugador.getMovx(), jugador.getMovy());
				}
				for (int j = 0; j < listaEnemigos.size(); j++) {
					if(!finJuego(listaEnemigos.get(j), jugador)) {
						
					}
					else {
						if(jugador.getSalud()>=0) {
							listaEnemigos.remove(j);
							jugador.perderVida();
						}else {
							mostrar=false;
							background = new Image("fin.jpg",1124, 620, true, false);
						}
					}
				}
				
				texto.guardarArchivo("Puntaje", puntaje);
				
				for (int j = 0; j < listaDisparos.size(); j++) {
					for (int i = 0; i < listaEnemigos.size(); i++) {
						if(destruirEnemigos(listaEnemigos.get(i),listaDisparos.get(j))) {
							listaEnemigos.remove(i);
							Enemigo temporal1 = new Enemigo((int)(Math.random()*1000),(int)(Math.random()*400));
							Enemigo temporal2 = new Enemigo((int)(Math.random()*1000),(int)(Math.random()*400));

							listaEnemigos.add(temporal1);
							listaEnemigos.add(temporal2);
							puntaje = puntaje + 10;
						}
					}
				}
				
				
			}
		}.start();
	}

	private void actualizar() {
		puntosSalida.setText(""+puntaje);
		
		if(jugador.getSalud()<1) {
			vida.setText("0");
		}
		vida.setText(""+jugador.getSalud());
	}


	@Override
	public void handle(KeyEvent event) { 
		if(event.getCode().equals(KeyCode.RIGHT)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			jugador.moverDerecha();
		}
		if (event.getCode().equals(KeyCode.LEFT)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			jugador.moverIzquierda();
		}
		if (event.getCode().equals(KeyCode.DOWN)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			jugador.moverAbajo();
		}
		if (event.getCode().equals(KeyCode.UP)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			jugador.moverArriba();
		}


		if (event.getCode().equals(KeyCode.W) && event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			if(listaDisparos.size()<5) {
				Disparo temporal = new Disparo(jugador.getMovx(),jugador.getMovy(),3);
				listaDisparos.add(temporal);
			}
		}
		if (event.getCode().equals(KeyCode.A)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			if(listaDisparos.size()<5) {
				Disparo temporal = new Disparo(jugador.getMovx(),jugador.getMovy(),2);
				listaDisparos.add(temporal);
			}
		}
		if (event.getCode().equals(KeyCode.S)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			if(listaDisparos.size()<5) {
				Disparo temporal = new Disparo(jugador.getMovx(),jugador.getMovy(),1);
				listaDisparos.add(temporal);
			}
		}
		if (event.getCode().equals(KeyCode.D)&& event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
			if(listaDisparos.size()<5) {
				Disparo temporal = new Disparo(jugador.getMovx(),jugador.getMovy(),0);
				listaDisparos.add(temporal);
			}
		}
	}		
}


