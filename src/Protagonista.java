import javafx.scene.image.Image;

public class Protagonista {
	private Image skin;
	private int movx;
	private int movy;
	private int posXmax;
	private int posYmax;
	private int puntaje;
	private int salud;
	
	public Protagonista() {
		skin = new Image("spaceship.png", 100, 150, true, false);
		this.movx = 0;
		this.movy = 0;
		salud=100;
		this.puntaje = 0;
	}
	
	public void moverAbajo() {
		movy = movy +10;
	}
	
	public int getSalud() {
		return salud;
	}

	public void perderVida() {
		salud=salud-5;
	}
	
	public void moverArriba() {
		movy = movy-10;
	}
	
	public void moverDerecha() {
		movx = movx+10;
	}
	
	public void moverIzquierda() {
		movx = movx-10;
	}
	
	public Image getSkin() {
		return skin;
	}

	public void setSkin(Image skin) {
		this.skin = skin;
	}

	public int getMovx() {
		return movx;
	}

	public void setMovx(int movx) {
		this.movx = movx;
	}

	public int getMovy() {
		return movy;
	}

	public void setMovy(int movy) {
		this.movy = movy;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}


	public int getPosXmax() {
		return movx + 100;
	}


	public void setPosXmax(int posXmax) {
		this.posXmax = posXmax;
	}


	public int getPosYmax() {
		return movy + 150;
	}


	public void setPosYmax(int posYmax) {
		this.posYmax = posYmax;
	}
	
}
