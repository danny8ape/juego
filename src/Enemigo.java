import javafx.scene.image.Image;

public class Enemigo {
	private Image skin;
	private int movx;
	private int movy;
	private int posXmax;
	private int posYmax;
	
	public Enemigo() {
	this.skin = new Image("Freddy.png", 50, 50, true, false);
	this.movx = 800;
	this.movy = 300;
	}
	
	public Enemigo(int posX, int posY) {
		this.skin = new Image("Freddy.png", 50, 50, true, false);
		this.movx = posX;
		this.movy =posY;
		}
		
	
	public void movimientoEnemigo(float moviXJugador, float movYJugador) {
		
		if(movx>moviXJugador ) {
			if( movy< movYJugador) {
			moverAbajoIzquierda();
			}
			else if(movy> movYJugador) {
			moverArribaIzquierda();
			}
			else {
				moverIzquierda();
			}
		}
		else if(movx<moviXJugador) {
			if( movy< movYJugador) {
			moverAbajoDerecha();
			}
			else if(movy> movYJugador) {
			moverArribaDerecha();
			}
			else {
				moverDerecha();
			}
		}
		else {
			if(movy<movYJugador) {
				moverAbajo();
			}
			else {
				moverArriba();
			}
		}
			
		}
	
	
	public void moverAbajoIzquierda() {
		movy = movy+1;
		movx = movx-1;
	}
	
	public void moverArribaIzquierda() {
		movy = movy-1;
		movx = movx-1;
	}
	
	public void moverAbajoDerecha() {
		movy = movy+1;
		movx = movx+1;
	}
	
	public void moverArribaDerecha() {
		movy = movy-1;
		movx = movx+1;
	}
	
	public void moverAbajo() {
		movy = movy +1;
	}
	
	public void moverArriba() {
		movy = movy-1;
	}
	
	public void moverDerecha() {
		movx = movx+1;
	}
	
	public void moverIzquierda() {
		movx = movx-1;
	}

	public Image getSkin() {
		return skin;
	}

	public void setSkin(Image skin) {
		this.skin = skin;
	}

	public int getMovx() {
		return movx;
	}

	public void setMovx(int movx) {
		this.movx = movx;
	}

	public int getMovy() {
		return movy;
	}

	public void setMovy(int movy) {
		this.movy = movy;
	}


	public int getPosXmax() {
		return movx + 80;
	}


	public void setPosXmax(int posXmax) {
		this.posXmax = posXmax;
	}


	public int getPosYmax() {
		return movy + 50;
	}


	public void setPosYmax(int posYmax) {
		this.posYmax = posYmax;
	}
	
	public void limite() {
		Math.sqrt(Math.pow(posXmax-movx, 2) + Math.pow(posYmax-movy, 2));
	}
}
